# sylius

Open Source eCommerce Framework on top of Symfony https://sylius.com

* [*Why many people are going to migrate from Magento 1 to Sylius?*](https://medium.com/@BitBag/why-many-people-are-going-to-migrate-from-magento-1-to-sylius-27a988122cd3) 2019
* [*Sylius for Magento and PHP Developers*](https://alanstorm.com/series/sylius-for-magento-and-php-developers/) 2019